#ifndef MAINWORKER_H
#define MAINWORKER_H

#include "client.h"
#include "yap.h"

#include <QObject>
#include <QByteArray>
#include <QBuffer>

#define DEFAULT_CHANNELS        1
#define SAMPLE_SIZE             16
#define DEFAULT_SAMPLE_RATE     48000

#define DEFAULT_ENCODE_BUFFER   4000
#define DEFAULT_DECODE_BUFFER   4000

class AudioSystem : public QObject {
    Q_OBJECT
public:
    explicit AudioSystem(Client *client, QObject *parent = 0);
    ~AudioSystem();

    void initInput();
    void initOutput();

public slots:
    void inputRead();
    void playAudio(const QVector<int16_t> *data);

private:
    Client *client;
    QIODevice *inputDevice;     // Provided by QAudioInput
    QIODevice *outputDevice;    // Provided by QAudioOutput
    QVector<int16_t> *loopbuffer;
};

#endif // MAINWORKER_H
