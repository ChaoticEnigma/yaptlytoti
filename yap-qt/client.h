#ifndef MAIN_H
#define MAIN_H

#include <QSettings>
//#include <QDialog>
#include <QAudioInput>
#include <QAudioOutput>
//#include <QMutex>
#include "yap.h"

// Settings Keys
#define AUDIO_INPUTDEVICE       "audio/inputDevice"
#define AUDIO_DEFAULTINPUT      "audio/defaultInput"
#define AUDIO_OUTPUTDEVICE      "audio/outputDevice"
#define AUDIO_DEFAULTOUTPUT     "audio/defaultOutput"

#define CAPTURE_CODEC           "codec/captureCodec"
#define CAPTURE_CODEC_DEFAULT   "Opus"

#define NETWORK_PORT            "network/port"
#define NETWORK_PORT_DEFAULT    7777

#define SERVERS                 "servers/list"
#define CURRENT_SERVER          "servers/current"

#define CURRENT_CHANNEL         "channels/current"

#define NOTIFY_MS 10

class AudioSystem;

struct Client {
    QSettings settings;
    QAudioInput *audioInput = nullptr;
    QAudioOutput *audioOutput = nullptr;
    //QMutex audioDeviceMutex;
    AudioSystem *audiosystem;
//    VoIP *voip;
};

#endif // MAIN_H
