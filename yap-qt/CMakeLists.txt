## YAPTLYTOTI CMakeLists.txt
cmake_minimum_required(VERSION 3.0)

project(Yap-Qt)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

find_package(Qt5 COMPONENTS Core Gui Qml Widgets Quick QuickControls2 Multimedia REQUIRED)

## SOURCES

set(Yap_SOURCES
    client.h

    main.cpp

    audiosystem.h
    audiosystem.cpp

    clientsettings.h
    clientsettings.cpp

    servers.h
    servers.cpp

    serverlistmodel.h
    serverlistmodel.cpp

    channels.h
    channels.cpp

    channellistmodel.h
    channellistmodel.cpp

    messages.h
    messages.cpp

    messagelistmodel.h
    messagelistmodel.cpp

    people.h
    people.cpp

    peoplelistmodel.h
    peoplelistmodel.cpp
)

set(Yap_RESOURCES
    resources.qrc
)

if(YAP_CLIENT)
    set(CLIENT_ALL)
else()
    set(CLIENT_ALL EXCLUDE_FROM_ALL)
endif()

## BUILD

add_executable(yap-qt ${CLIENT_ALL} ${Yap_SOURCES} ${Yap_RESOURCES})
target_compile_definitions(yap-qt PRIVATE BUILDING)
target_link_libraries(yap-qt libyap Qt5::Multimedia Qt5::Core Qt5::Gui Qt5::Widgets Qt5::Quick Qt5::QuickControls2)
