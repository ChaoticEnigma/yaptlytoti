#include "people.h"
#include "servers.h"
#include <QDebug>

People::People(Servers *servers, Client *aclient) : client(aclient)
{
    connect(servers, &Servers::serverChanged, this, [=](QString id) {
        updatePeopleList(id);
    });

    currentServer = client->settings.value(CURRENT_SERVER, "").toString();

    if (currentServer == "") {
        return;
    }

    getPeople();
}

void People::addPerson(QString id)
{
    // TODO do serverside
}

void People::updateItemAtIndex(int i, Person person)
{
    // not sure what the correct way to just update an item
    emit preItemRemoved(i, i);
    people.removeAt(i);
    emit postItemRemoved();
    emit preItemAppended(i, i);
    people.insert(i, person);
    emit postItemAppended();
}

void People::getPeople()
{
    // TODO do serverside

    people.append({"A", currentServer + "-fakeUserA"});
    people.append({"B", currentServer + "-fakeUserB"});
    people.append({"C", currentServer + "-fakeUserC"});
}

QVector<Person> People::items() const
{
    return people;
}

bool People::setItemAt(int index, const Person &person)
{
    if (index < 0 || index >= people.size()) {
        return false;
    }

    const Person &oldPerson = people[index];

    // return false if nothing has changed
    if (person.id == oldPerson.id &&
            person.name == oldPerson.name) {
        return false;
    }

    people[index] = person;
    return true;
}

void People::removePerson(QString id)
{
    // TODO do this serverside

    qDebug() << "Remove person: " << id;

    for (int i = 0; i < people.size();) {
        if (people[i].id == id) {
            emit preItemRemoved(i, i);

            people.removeAt(i);

            emit postItemRemoved();
        } else {
            i++;
        }
    }
}

void People::updatePeopleList(QString id)
{
    currentServer = id;

    // TODO fetch people from server serverside

    if (!people.isEmpty()) {
        emit preItemRemoved(0, people.size() - 1);

        people = {};

        emit postItemRemoved();
    }

    if (currentServer != "") {
        emit preItemAppended(0, 2);

        getPeople();

        emit postItemAppended();
    }
}
