#ifndef PEOPLELISTMODEL_H
#define PEOPLELISTMODEL_H

#include <QAbstractListModel>

class People;

class PeopleListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(People *people READ people WRITE setPeople)

public:
    explicit PeopleListModel(QObject *parent = nullptr);

    enum {
        IDRole,
        NameRole,
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    People *people() const;
    void setPeople(People *);

private:
    People *peopleList;
};

#endif // PEOPLELISTMODEL_H
