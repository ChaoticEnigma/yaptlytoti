#ifndef SERVERS_H
#define SERVERS_H

#include "client.h"
#include <QVector>

struct Server
{
    QString id;
    QString name;
    QString thumbnail;
    bool selected;
};

class Servers : public QObject
{
    Q_OBJECT
public:
    explicit Servers(Client *client);

    // allow qml to get servers
    QVector<Server> items() const;

    // allow model to modify servers list
    bool setItemAt(int index, const Server &server);

signals:
    // qml signals
    void preItemAppended(int index);
    void postItemAppended();

    void preItemRemoved(int index);
    void postItemRemoved();

    // yap signals
    void serverChanged(QString id);

public slots:
    void addServer(QString);
    void removeServer(QString);
    void setCurrentServer(QString);

private:
    Client *client;
    QVector<Server> servers;

    void saveServerList();
    void updateItemAtIndex(int, Server);
    void emitServerChanged(QString);
};

#endif // SERVERS_H
