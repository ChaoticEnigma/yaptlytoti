#ifndef SERVERLISTMODEL_H
#define SERVERLISTMODEL_H

#include <QAbstractListModel>

class Servers;

class ServerListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(Servers *servers READ servers WRITE setServers)

public:
    explicit ServerListModel(QObject *parent = nullptr);

    enum {
        IDRole,
        NameRole,
        ThumbnailRole,
        SelectedRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    Servers *servers() const;
    void setServers(Servers *);

private:
    Servers *serverList;
};

#endif // SERVERLISTMODEL_H
