#include "clientsettings.h"

#include <QDebug>
#include <QAudioDeviceInfo>

#include "audiosystem.h"

#define DEFAULT_DEVICE "Default"

ClientSettings::ClientSettings(Client *aclient) : client(aclient){
//    // Select capture codec
//    QString codecstr = client->settings.value(CAPTURE_CODEC, CAPTURE_CODEC_DEFAULT).toString();
//    ui->captureCodecComboBox->setCurrentText(codecstr);
//    captureCodecChanged(codecstr);
//    connect(ui->captureCodecComboBox, SIGNAL(currentTextChanged(QString)), this, SLOT(captureCodecChanged(QString)));
}

ClientSettings::~ClientSettings(){
}

QStringList ClientSettings::getInputDevices()
{
    QStringList devices;
    // Get available audio input devices
    devices << DEFAULT_DEVICE;
    foreach(const QAudioDeviceInfo &deviceInfo, QAudioDeviceInfo::availableDevices(QAudio::AudioInput)){
        devices << deviceInfo.deviceName();
    }
    return devices;
}

QList<QAudioDeviceInfo> ClientSettings::getInputDevicesInfo()
{
    QList<QAudioDeviceInfo> devices;
    // Get available audio input devices
    foreach(const QAudioDeviceInfo &deviceInfo, QAudioDeviceInfo::availableDevices(QAudio::AudioInput)){
        devices << deviceInfo;
    }
    return devices;
}

QStringList ClientSettings::getOutputDevices()
{
    QStringList devices;
    // Get available audio input devices
    devices << DEFAULT_DEVICE;
    foreach(const QAudioDeviceInfo &deviceInfo, QAudioDeviceInfo::availableDevices(QAudio::AudioOutput)){
        devices << deviceInfo.deviceName();
    }
    return devices;
}

QList<QAudioDeviceInfo> ClientSettings::getOutputDevicesInfo()
{
    QList<QAudioDeviceInfo> devices;
    // Get available audio input devices
    foreach(const QAudioDeviceInfo &deviceInfo, QAudioDeviceInfo::availableDevices(QAudio::AudioOutput)){
        devices << deviceInfo;
    }
    return devices;
}

int ClientSettings::getCurrentInputDeviceIndex()
{
    if(client->settings.value(AUDIO_DEFAULTINPUT, false).toBool()){
        // Select default device
        return 0;
    } else {
        // Select device by name
        auto inputName = client->settings.value(AUDIO_INPUTDEVICE, DEFAULT_DEVICE).toString();
        auto index = getInputDevices().indexOf(inputName);
        if (index == -1) {
            // Requested device not in list
            return 1;
        } else {
            return index;
        }
    }
}

int ClientSettings::getCurrentOutputDeviceIndex()
{
    if(client->settings.value(AUDIO_DEFAULTOUTPUT, false).toBool()){
        // Select default device
        return 0;
    } else {
        // Select device by name
        auto inputName = client->settings.value(AUDIO_OUTPUTDEVICE, DEFAULT_DEVICE).toString();
        auto index = getInputDevices().indexOf(inputName);
        if (index == -1) {
            // Requested device not in list
            return 1;
        } else {
            return index;
        }
    }
}

void ClientSettings::setCurrentInputDeviceIndex(int idx)
{
    // Check if the default device is selected
    if(idx == 0){
        inputDeviceInfo = QAudioDeviceInfo::defaultInputDevice();
        client->settings.setValue(AUDIO_DEFAULTINPUT, true);
    } else {
        // -1 since the list does not contain the default device
        inputDeviceInfo = getInputDevicesInfo()[idx - 1];
        client->settings.setValue(AUDIO_DEFAULTINPUT, false);
        client->settings.setValue(AUDIO_INPUTDEVICE, inputDeviceInfo.deviceName());
    }
    qInfo() << "Input Device:" << inputDeviceInfo.deviceName() << (client->settings.value(AUDIO_DEFAULTINPUT).toBool() ? "(default)" : "");

    // Format
    QAudioFormat format = inputDeviceInfo.preferredFormat();
    format.setCodec("audio/pcm");
    format.setChannelCount(DEFAULT_CHANNELS);
    format.setSampleRate(DEFAULT_SAMPLE_RATE);
    format.setSampleSize(SAMPLE_SIZE);
    format.setSampleType(QAudioFormat::SignedInt);
    format.setByteOrder(QAudioFormat::LittleEndian);
    qInfo() << "IFS" << inputDeviceInfo.isFormatSupported(format);

    // Open input device
    delete client->audioInput;
    //client->audioInput = new QAudioInput(inputDeviceInfo, inputDeviceInfo.preferredFormat());
    client->audioInput = new QAudioInput(inputDeviceInfo, format);

    qDebug() << "Input Format:"
            << client->audioInput->format().codec()
            << client->audioInput->format().channelCount() << "channels"
            << client->audioInput->format().sampleRate() << "Hz"
            << client->audioInput->format().sampleSize() << "bit"
            << client->audioInput->format().sampleType()
            << client->audioInput->format().byteOrder();

    client->audiosystem->initInput();
}

void ClientSettings::setCurrentOutputDeviceIndex(int idx){
    // Check if the default device is selected
    if(idx == 0){
        outputDeviceInfo = QAudioDeviceInfo::defaultOutputDevice();
        client->settings.setValue(AUDIO_DEFAULTOUTPUT, true);
    } else {
        // -1 since the list does not contain the default device
        outputDeviceInfo = getOutputDevicesInfo()[idx - 1];
        client->settings.setValue(AUDIO_DEFAULTOUTPUT, false);
        client->settings.setValue(AUDIO_OUTPUTDEVICE, outputDeviceInfo.deviceName());
    }
    qDebug() << "Output Device:" << outputDeviceInfo.deviceName() << (client->settings.value(AUDIO_DEFAULTOUTPUT).toBool() ? "(default)" : "");

    // Format
    QAudioFormat format = outputDeviceInfo.preferredFormat();
    format.setCodec("audio/pcm");
    format.setChannelCount(DEFAULT_CHANNELS);
    format.setSampleRate(DEFAULT_SAMPLE_RATE);
    format.setSampleSize(SAMPLE_SIZE);
    format.setSampleType(QAudioFormat::SignedInt);
    format.setByteOrder(QAudioFormat::LittleEndian);
    qInfo() << "OFS" << outputDeviceInfo.isFormatSupported(format);

    // Open output device
    delete client->audioOutput;
    //client->audioOutput = new QAudioOutput(outputDeviceInfo, outputDeviceInfo.preferredFormat());
    client->audioOutput = new QAudioOutput(outputDeviceInfo, format);

    qDebug() << "Output Format:"
            << client->audioOutput->format().codec()
            << client->audioOutput->format().channelCount() << "channels"
            << client->audioOutput->format().sampleRate() << "Hz"
            << client->audioOutput->format().sampleSize() << "bit"
            << client->audioOutput->format().sampleType()
            << client->audioOutput->format().byteOrder();

    client->audiosystem->initOutput();
}

void ClientSettings::captureCodecChanged(QString text){
    qDebug() << "Capture Codec:" << text;
    client->settings.setValue(CAPTURE_CODEC, text);

    QMap<QString, yap_codec_type_t> cmap = {
        { "Raw",    YAP_CODEC_PCM },
        { "Opus",   YAP_CODEC_OPUS },
    };

//    client->voip->initCodec(cmap[text]);
}
