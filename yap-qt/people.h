#ifndef PEOPLE_H
#define PEOPLE_H

#include "client.h"
#include <QVector>

class Servers;

struct Person
{
    QString id;
    QString name;
};

class People : public QObject
{
    Q_OBJECT
public:
    explicit People(Servers *servers, Client *client);

    // allow qml to get peoples
    QVector<Person> items() const;

    // allow model to modify peoples list
    bool setItemAt(int index, const Person &person);

signals:
    // qml signals
    void preItemAppended(int start, int end);
    void postItemAppended();

    void preItemRemoved(int start, int end);
    void postItemRemoved();

public slots:
    void addPerson(QString);
    // server admins only
    void removePerson(QString);

private:
    Client *client;

    QVector<Person> people;

    // server id
    void updatePeopleList(QString id);
    void updateItemAtIndex(int, Person);
    void getPeople();

    QString currentServer;

    // remove when server is real
    int testTmpId = 0;
};

#endif // PEOPLE_H
