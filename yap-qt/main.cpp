#include "client.h"
#include "clientsettings.h"
#include "audiosystem.h"
#include "servers.h"
#include "serverlistmodel.h"
#include "channels.h"
#include "channellistmodel.h"
#include "messages.h"
#include "messagelistmodel.h"
#include "people.h"
#include "peoplelistmodel.h"
#include <QApplication>
#include <QThread>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
#include <QQuickStyle>
#include <QQuickWindow>
#endif

int main(int argc, char **argv){
    Q_INIT_RESOURCE(resources);

    QApplication app(argc, argv);
    QApplication::setOrganizationName("Zennix Studios");
    QApplication::setApplicationName("YapTlyToti");

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

#if QT_VERSION >= QT_VERSION_CHECK(5, 10, 0)
    // Linux desktops (particularly Plasma) may have pre-built styles,
    // so we will set the fallback appropriately until we have our own themes.
    QQuickStyle::setFallbackStyle("Fusion");
    // Removes the "blurry" text for windows, and based on the doc's, this is preferred
    // for my case since this app is not doing anything special with text.
    QQuickWindow::setTextRenderType(QQuickWindow::NativeTextRendering);
#endif

    QIcon::setThemeName("breeze");

    QScopedPointer<Client> client(new Client);

    // Create audio I/O controller
    QScopedPointer<AudioSystem> audiosystem(new AudioSystem(client.data()));
    client->audiosystem = audiosystem.data();

    // Create settings, triggers audio setup
    QScopedPointer<ClientSettings> clientsettings(new ClientSettings(client.data()));
    QScopedPointer<Servers> servers(new Servers(client.data()));
    QScopedPointer<Channels> channels(new Channels(servers.data(), client.data()));
    QScopedPointer<People> people(new People(servers.data(), client.data()));
    QScopedPointer<Messages> messages(new Messages(channels.data(), client.data()));

    qmlRegisterType<ServerListModel>("ServerList", 1, 0, "ServerListModel");
    qmlRegisterUncreatableType<Servers>("ServerList", 1, 0, "ServerList",
                                        "ServerList should not be created in QML");

    qmlRegisterType<ChannelListModel>("ChannelList", 1, 0, "ChannelListModel");
    qmlRegisterUncreatableType<Channels>("ChannelList", 1, 0, "ChannelList",
                                         "ChannelList should not be created in QML");

    qmlRegisterType<PeopleListModel>("PeopleList", 1, 0, "PeopleListModel");
    qmlRegisterUncreatableType<People>("PeopleList", 1, 0, "PeopleList",
                                       "PeopleList should not be created in QML");

    qmlRegisterType<MessageListModel>("MessageList", 1, 0, "MessageListModel");
    qmlRegisterUncreatableType<Messages>("MessageList", 1, 0, "MessageList",
                                         "MessageList should not be created in QML");

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("clientsettingsctx", clientsettings.data());
    engine.rootContext()->setContextProperty("serversctx", servers.data());
    engine.rootContext()->setContextProperty("channelsctx", channels.data());
    engine.rootContext()->setContextProperty("messagesctx", messages.data());
    engine.rootContext()->setContextProperty("peoplectx", people.data());

    engine.load(QUrl(QStringLiteral("qrc:/ui/main.qml")));
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    // Create codec/network controller
//    client->voip = new VoIP();

    int ret = app.exec();

    return ret;
}
