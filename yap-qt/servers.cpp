#include "servers.h"
#include <QDebug>

Servers::Servers(Client *aclient) : client(aclient)
{
    auto savedServers = client->settings.value(SERVERS, QStringList({"None"})).toStringList();
    auto curServer = client->settings.value(CURRENT_SERVER, "").toString();

    qDebug() << "Current server: " << curServer;

    for (auto server: savedServers) {
        // TODO translate id to name by fetching info from server
        servers.append({server, server, "TODO", server == curServer});
    }
}

void Servers::setCurrentServer(QString id)
{
    qDebug() << "Setting current server to: " << id;

    for (int i = 0; i < servers.size(); i++) {
        auto server = servers[i];
        if (server.selected && server.id != id) {
            server.selected = false;
            updateItemAtIndex(i, server);
        }
        if (!server.selected && server.id == id) {
            server.selected = true;
            updateItemAtIndex(i, server);
            emitServerChanged(id);
        }
    }

    saveServerList();
}

void Servers::saveServerList()
{
    QStringList newServerList;
    for (auto server : servers) {
        newServerList << server.id;
    }
    client->settings.setValue(SERVERS, newServerList);
}

void Servers::updateItemAtIndex(int i, Server server)
{
    // not sure what the correct way to just update an item
    emit preItemRemoved(i);
    servers.removeAt(i);
    emit postItemRemoved();
    emit preItemAppended(i);
    servers.insert(i, server);
    emit postItemAppended();
}

void Servers::emitServerChanged(QString id)
{
    client->settings.setValue(CURRENT_SERVER, id);
    emit serverChanged(id);
}

QVector<Server> Servers::items() const
{
    return servers;
}

bool Servers::setItemAt(int index, const Server &server)
{
    if (index < 0 || index >= servers.size()) {
        return false;
    }

    const Server &oldServer = servers[index];

    // return false if nothing has changed
    if (server.id == oldServer.id &&
            server.name == oldServer.name &&
            server.thumbnail == oldServer.thumbnail &&
            server.selected == oldServer.selected) {
        return false;
    }

    servers[index] = server;
    return true;
}

void Servers::addServer(QString id)
{
    // don't add a server that already exists
    for (auto server : servers) {
        if (server.id == id) {
            return;
        }
    }

    emit preItemAppended(servers.size());

    Server server;
    server.selected = false;
    server.id = id;

    // TODO get this by asking server
    server.name = id;
    server.thumbnail = "TODO";

    servers.append(server);

    emit postItemAppended();

    // just set the current server to the one they are adding
    setCurrentServer(id);
}

void Servers::removeServer(QString id)
{
    for (int i = 0; i < servers.size();) {
        if (servers[i].id == id) {
            // if removing server currently active,
            // then select a different server
            if (servers[i].selected) {
                // select 1st server, but
                // if the 1st server is being removed
                // then select 2nd server if available
                if (i != 0) {
                    setCurrentServer(servers[0].id);
                } else if (servers.size() > 1) {
                    setCurrentServer(servers[1].id);
                } else {
                    emitServerChanged("");
                }
            }

            emit preItemRemoved(i);

            servers.removeAt(i);

            emit postItemRemoved();
        } else {
            i++;
        }
    }

    saveServerList();
}
