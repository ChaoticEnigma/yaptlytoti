#include "messagelistmodel.h"
#include "messages.h"

MessageListModel::MessageListModel(QObject *parent)
    : QAbstractListModel(parent)
    , messageList(nullptr)
{
}

int MessageListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !messageList)
        return 0;

    return messageList->items().size();
}

QVariant MessageListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !messageList)
        return QVariant();

    const Message message = messageList->items()[index.row()];
    switch(role) {
    case IDRole:
        return QVariant(message.id);
    case NameRole:
        return QVariant(message.name);
    case TextRole:
        return QVariant(message.text);
    case DateRole:
        return QVariant(message.date);
    }
    return QVariant();
}

bool MessageListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!messageList) {
        return false;
    }

    Message message = messageList->items()[index.row()];
    switch(role) {
    case IDRole:
        message.id = value.toString();
        break;
    case NameRole:
        message.name = value.toString();
        break;
    case TextRole:
        message.text = value.toString();
        break;
    case DateRole:
        message.date = value.toBool();
        break;
    }

    if (messageList->setItemAt(index.row(), message)) {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags MessageListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> MessageListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[IDRole] = "id";
    names[NameRole] = "name";
    names[TextRole] = "text";
    names[DateRole] = "date";
    return names;
}

Messages *MessageListModel::messages() const
{
    return messageList;
}

void MessageListModel::setMessages(Messages *newMessages)
{
    beginResetModel();

    if (messageList) {
        messageList->disconnect(this);
    }

    messageList = newMessages;

    if (messageList) {
        connect(messageList, &Messages::preItemAppended, this, [=](int start, int end) {
            beginInsertRows(QModelIndex(), start, end);
        });
        connect(messageList, &Messages::postItemAppended, this, [=]() {
            endInsertRows();
            emit onMessagesChanged();
        });

        connect(messageList, &Messages::preItemRemoved, this, [=](int start, int end) {
            beginRemoveRows(QModelIndex(), start, end);
        });
        connect(messageList, &Messages::postItemRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}
