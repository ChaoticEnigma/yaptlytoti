#include "messages.h"
#include "channels.h"
#include <QDebug>

Messages::Messages(Channels *channels, Client *aclient) : client(aclient)
{
    connect(channels, &Channels::channelChanged, this, [=](QString id) {
        updateMessageList(id);
    });

    QString currentServer = client->settings.value(CURRENT_SERVER, "").toString();
    currentChannels = client->settings.value(CURRENT_CHANNEL, {}).toMap();
    currentChannel = currentChannels[currentServer].toString();

    if (currentChannel == "") {
        return;
    }

    getMessages();
}

void Messages::addMessage(QString text)
{
    // TODO do serverside

    if (text == "") {
        return;
    }

    Message message;
    message.id = "TODO" + QString(testTmpId++);
    message.name = "YOU!";
    message.text = text;
    message.date = "11:11";

    emit preItemAppended(messages.size(), messages.size());

    messages.append(message);

    emit postItemAppended();
}

void Messages::updateItemAtIndex(int i, Message message)
{
    // not sure what the correct way to just update an item
    emit preItemRemoved(i, i);
    messages.removeAt(i);
    emit postItemRemoved();
    emit preItemAppended(i, i);
    messages.insert(i, message);
    emit postItemAppended();
}

void Messages::getMessages()
{
    // TODO do serverside

    messages.append({"A", "fakeUserA", currentChannel + "-A message", "00:00"});
    messages.append({"B", "fakeUserB", currentChannel + "-B message", "00:01"});
    messages.append({"C", "fakeUserC", currentChannel + "-C message", "00:02"});
}

QVector<Message> Messages::items() const
{
    return messages;
}

bool Messages::setItemAt(int index, const Message &message)
{
    if (index < 0 || index >= messages.size()) {
        return false;
    }

    const Message &oldMessage = messages[index];

    // return false if nothing has changed
    if (message.id == oldMessage.id &&
            message.name == oldMessage.name &&
            message.text == oldMessage.text &&
            message.date == oldMessage.date) {
        return false;
    }

    messages[index] = message;
    return true;
}

void Messages::removeMessage(QString id)
{
    // TODO do this serverside

    qDebug() << "Remove message: " << id;

    for (int i = 0; i < messages.size();) {
        if (messages[i].id == id) {
            emit preItemRemoved(i, i);

            messages.removeAt(i);

            emit postItemRemoved();
        } else {
            i++;
        }
    }
}

void Messages::editMessage(QString id, QString text)
{
    // TODO do serverside

    qDebug() << "Edit message: " << id;

    for (int i = 0; i < messages.size(); i++) {
        auto message = messages[i];
        if (message.id == id) {
            message.text = text;
            updateItemAtIndex(i, message);
            break;
        }
    }
}

void Messages::updateMessageList(QString id)
{
    currentChannel = id;

    // TODO fetch messages from channel serverside

    if (!messages.isEmpty()) {
        emit preItemRemoved(0, messages.size() - 1);

        messages = {};

        emit postItemRemoved();
    }

    if (currentChannel != "") {
        emit preItemAppended(0, 2);

        getMessages();

        emit postItemAppended();
    }
}
