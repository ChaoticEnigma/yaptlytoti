#ifndef CHANNELLISTMODEL_H
#define CHANNELLISTMODEL_H

#include <QAbstractListModel>

class Channels;

class ChannelListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(Channels *channels READ channels WRITE setChannels)

public:
    explicit ChannelListModel(QObject *parent = nullptr);

    enum {
        IDRole,
        NameRole,
        SelectedRole,
        VoiceEnabledRole,
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    Channels *channels() const;
    void setChannels(Channels *);

private:
    Channels *channelList;
};

#endif // CHANNELLISTMODEL_H
