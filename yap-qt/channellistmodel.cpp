#include "channellistmodel.h"
#include "channels.h"

ChannelListModel::ChannelListModel(QObject *parent)
    : QAbstractListModel(parent)
    , channelList(nullptr)
{
}

int ChannelListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !channelList)
        return 0;

    return channelList->items().size();
}

QVariant ChannelListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !channelList)
        return QVariant();

    const Channel channel = channelList->items()[index.row()];
    switch(role) {
    case IDRole:
        return QVariant(channel.id);
    case NameRole:
        return QVariant(channel.name);
    case SelectedRole:
        return QVariant(channel.selected);
    case VoiceEnabledRole:
        return QVariant(channel.voiceEnabled);
    }
    return QVariant();
}

bool ChannelListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!channelList) {
        return false;
    }

    Channel channel = channelList->items()[index.row()];
    switch(role) {
    case IDRole:
        channel.id = value.toString();
        break;
    case NameRole:
        channel.name = value.toString();
        break;
    case SelectedRole:
        channel.selected = value.toBool();
        break;
    case VoiceEnabledRole:
        channel.voiceEnabled = value.toBool();
        break;
    }

    if (channelList->setItemAt(index.row(), channel)) {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ChannelListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> ChannelListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[IDRole] = "id";
    names[NameRole] = "name";
    names[SelectedRole] = "selected";
    names[VoiceEnabledRole] = "voiceEnabled";
    return names;
}

Channels *ChannelListModel::channels() const
{
    return channelList;
}

void ChannelListModel::setChannels(Channels *newChannels)
{
    beginResetModel();

    if (channelList) {
        channelList->disconnect(this);
    }

    channelList = newChannels;

    if (channelList) {
        connect(channelList, &Channels::preItemAppended, this, [=](int start, int end) {
            beginInsertRows(QModelIndex(), start, end);
        });
        connect(channelList, &Channels::postItemAppended, this, [=]() {
            endInsertRows();
        });

        connect(channelList, &Channels::preItemRemoved, this, [=](int start, int end) {
            beginRemoveRows(QModelIndex(), start, end);
        });
        connect(channelList, &Channels::postItemRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}
