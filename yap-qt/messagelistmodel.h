#ifndef MESSAGELISTMODEL_H
#define MESSAGELISTMODEL_H

#include <QAbstractListModel>

class Messages;

class MessageListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(Messages *messages READ messages WRITE setMessages NOTIFY onMessagesChanged)

public:
    explicit MessageListModel(QObject *parent = nullptr);

    enum {
        IDRole,
        NameRole,
        TextRole,
        DateRole
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;

    Messages *messages() const;
    void setMessages(Messages *);

signals:
    void onMessagesChanged();

private:
    Messages *messageList;
};

#endif // MESSAGELISTMODEL_H
