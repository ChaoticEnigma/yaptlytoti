#ifndef CHANNELS_H
#define CHANNELS_H

#include "client.h"
#include <QVector>

class Servers;

struct Channel
{
    QString id;
    QString name;
    bool selected;
    bool voiceEnabled;
};

class Channels : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString currentChannel READ currentChannel NOTIFY channelChanged)
public:
    explicit Channels(Servers *servers, Client *client);

    // allow qml to get channels
    QVector<Channel> items() const;
    // used for the channel title in header
    QString currentChannel() const;

    // allow model to modify channels list
    bool setItemAt(int index, const Channel &channel);

signals:
    // qml signals
    void preItemAppended(int start, int end);
    void postItemAppended();

    void preItemRemoved(int start, int end);
    void postItemRemoved();

    // yap and qml signals
    void channelChanged(QString id);

public slots:
    void setCurrentChannel(QString);
    // server admins only
    void addChannel(QString);
    // server admins only
    void removeChannel(QString);

private:
    Client *client;

    QVector<Channel> channels;
    // current channel
    QString channel;

    // server id
    void updateChannelList(QString id);
    void updateItemAtIndex(int, Channel);
    void getChannels();
    void emitCurrentChannel(QString id, QString name);

    QString currentServer;
};

#endif // CHANNELS_H
