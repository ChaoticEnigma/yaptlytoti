import QtQuick 2.0
import QtQuick.Layouts 1.3

RowLayout {
    Layout.fillHeight: true
    Layout.fillWidth: false

    Servers {
        Layout.fillHeight: true
    }

    Channels {
        Layout.fillWidth: true
        Layout.fillHeight: true
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
