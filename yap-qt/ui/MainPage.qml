import QtQuick 2.0
import QtQuick.Layouts 1.3

// visible: root.width > minPeopleWidth
// visible does not seem to affect anchors, so set width to 0

Item {
    id: element

    ServerChannel {
        id: channels
        width: root.width > minChannelWidth ? 250 : 0
        visible: root.width > minChannelWidth
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.topMargin: 0
    }

    People {
        id: people
        width: root.width > minPeopleWidth ? 150 : 0
        visible: root.width > minPeopleWidth
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        Layout.fillWidth: false
        Layout.fillHeight: true
    }

    Chat {
        id: chat
        anchors.right: people.left
        anchors.left: channels.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.topMargin: 0
    }
}







/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:2;anchors_height:0}D{i:1;anchors_height:100;anchors_width:100}
}
 ##^##*/
