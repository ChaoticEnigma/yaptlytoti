import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

ScrollView {
    id: scrollView
    padding: 10

    GridLayout {
        columns: 2
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top

        Label {
            text: "Codec"
        }

        ComboBox {
            Layout.fillWidth: true
            model: ListModel {
                id: inputItems
                ListElement {
                    text: "Opus Voice";
                }
                ListElement {
                    text: "Opus Music";
                }
            }
        }

        Label {
            text: qsTr("Voice Enabled")
        }

        CheckBox {
            id: voiceEnabled
        }
    }


    Button {
        text: qsTr("Ok")
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        onClicked: root.setVisibleView(channelSettings)
    }
}


/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
