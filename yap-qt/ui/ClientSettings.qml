import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

ScrollView {
    id: scrollView
    padding: 10

    GridLayout {
        columns: 2
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top

        Label {
            text: "Input Device"
        }

        ComboBox {
            Layout.fillWidth: true
            model: clientsettingsctx.getInputDevices()
            currentIndex: clientsettingsctx.getCurrentInputDeviceIndex();
            onCurrentIndexChanged: clientsettingsctx.setCurrentInputDeviceIndex(currentIndex);
        }

        Label {
            text: "Output Device"
        }

        ComboBox {
            Layout.fillWidth: true
            model: clientsettingsctx.getOutputDevices()
            currentIndex: clientsettingsctx.getCurrentOutputDeviceIndex();
            onCurrentIndexChanged: clientsettingsctx.setCurrentOutputDeviceIndex(currentIndex);
        }
    }

    Button {
        id: button
        text: qsTr("Ok")
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        onClicked: root.setVisibleView(clientSettings)
    }
}















/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
