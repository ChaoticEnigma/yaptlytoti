import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2
import QtQuick.Window 2.0

ApplicationWindow {
    id: root
    visible: true
    width: 640
    height: 480
    minimumWidth: 300
    minimumHeight: 200

    // When to show components
    property int minChannelWidth: 500
    property int minPeopleWidth: 700

    function setVisibleView(view) {
        if (view.visible) {
            view.visible = false
            defaultView.visible = true
        } else {
            defaultView.visible = false
            clientSettings.visible = false
            peopleView.visible = false
            channelView.visible = false
            channelSettings.visible = false

            view.visible = true
        }
    }

    onWidthChanged: {
        if (channelView.visible && width > minChannelWidth) {
            channelView.visible = false
            defaultView.visible = true
        }
        if (peopleView.visible && width > minPeopleWidth) {
            peopleView.visible = false
            defaultView.visible = true
        }
    }

    ColumnLayout {
        id: columnLayout
        anchors.fill: parent

        MainHeader {
            id: header;
            Layout.fillWidth: true
        }

        MainPage {
            id: defaultView
            Layout.fillWidth: true
            Layout.fillHeight: true
        }

        ServerChannel {
            id: channelView
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: false
        }

        People {
            id: peopleView
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: false
        }

        ClientSettings {
            id: clientSettings
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: false
        }

        ChannelSettings {
            id: channelSettings
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: false
        }

        AddServer {
            id: addServer
            Layout.fillHeight: true
            Layout.fillWidth: true
            visible: false
        }
    }

}





/*##^## Designer {
    D{i:1;anchors_height:100;anchors_width:100}
}
 ##^##*/
