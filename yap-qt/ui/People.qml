import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

import PeopleList 1.0

ScrollView {
    ListView {
        anchors.fill: parent
        clip: true
        model: PeopleListModel {
            people: peoplectx
        }

        delegate: Person {
            width: parent.width
        }
    }
}
