import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

ScrollView {
    padding: 10

    GridLayout {
        columns: 2
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top

        Label {
            text: "Server ID"
        }

        TextField {
            id: serverId
            Layout.fillWidth: true
        }

    }

    Button {
        id: ok
        text: qsTr("Ok")
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        onClicked: {
            serversctx.addServer(serverId.text)
            root.setVisibleView(addServer)
        }
    }

    Button {
        text: qsTr("Cancel")
        anchors.rightMargin: 10
        anchors.bottomMargin: 0
        anchors.right: ok.left
        anchors.bottom: parent.bottom
        onClicked: root.setVisibleView(addServer)
    }
}


/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
