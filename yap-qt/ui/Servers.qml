import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

import ServerList 1.0

ScrollView {
    id: scrollView
    width: 80

    ToolBar {
        id: footer
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.topMargin: 0
        ToolButton {
            anchors.horizontalCenter: parent.horizontalCenter
            onClicked: root.setVisibleView(addServer)

            Image {
                anchors.centerIn: parent
                source: "list-add"
            }
        }
    }

    ListView {
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: footer.top
        clip: true
        model: ServerListModel {
            servers: serversctx
        }
        delegate: ToolButton {
            anchors.horizontalCenter: parent.horizontalCenter
            text: model.selected ? ("[" + model.name + "]") : model.name

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: {
                    if (mouse.button === Qt.RightButton) {
                        menu.popup()
                    } else {
                        serversctx.setCurrentServer(model.id)
                    }
                }
            }

            Menu {
                id: menu
                MenuItem {
                    text: "Remove Server"
                    onClicked: {
                        serversctx.removeServer(model.id)
                    }
                }
            }

//            Image {
//                anchors.centerIn: parent
//                source: model.thumbnail
//            }
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
