import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

Item {
    id: chatItem
    width: parent.width
    height: message.height + ok.height + 20

    property string originalMessage: ""

    function closeEdit() {
        message.readOnly = true
        ok.visible = false
        ok.height = 0
        cancel.visible = false
        cancel.height = 0
    }

    function editMessage() {
        closeEdit()
        messagesctx.editMessage(model.id, message.text)
    }

    function cancelMessage() {
        closeEdit()
        message.text = originalMessage
    }

    Label {
        id: time
        text: model.date
        opacity: 0.5
    }

    Label {
        id: name
        text: model.name
        anchors.leftMargin: 10
        anchors.left: time.right
        font.bold: true
    }

    TextEdit {
        id: message
        text: model.text
        anchors.right: configure.left
        anchors.left: parent.left
        anchors.top: time.bottom
        Layout.fillWidth: true
        wrapMode: Text.WordWrap
        readOnly: true
        selectByMouse: true

        Keys.onPressed: {
            if (event.key === Qt.Key_Enter || event.key === Qt.Key_Return) {
                editMessage()
                event.accepted = true
            } else if (event.key === Qt.Key_Escape) {
                cancelMessage()
                event.accepted = true
            }
        }
    }

    ToolButton {
        id: configure
        anchors.right: parent.right
        onClicked: menu.popup()

        Image {
            anchors.centerIn: parent
            source: "configure"
        }

        Menu {
            id: menu
            MenuItem {
                text: "Edit Message"
                onClicked: {
                    originalMessage = message.text
                    message.readOnly = false
                    ok.visible = true
                    cancel.visible = true
                    // reset height
                    ok.height = undefined
                    cancel.height = undefined
                    message.focus = true
                }
            }

            MenuItem {
                text: "Remove Message"
                onClicked: messagesctx.removeMessage(model.id)
            }
        }
    }

    Button {
        id: ok
        anchors.right: parent.right
        visible: false
        text: "Ok"
        anchors.top: message.bottom
        // so that chatitem is not larger than it needs to be
        height: 0
        onClicked: editMessage()
    }

    Button {
        id: cancel
        anchors.right: ok.left
        visible: false
        text: "Cancel"
        anchors.rightMargin: 10
        anchors.top: message.bottom
        // so that chatitem is not larger than it needs to be
        height: 0
        onClicked: cancelMessage()
    }
}











/*##^## Designer {
    D{i:2;anchors_x:33}D{i:3;anchors_width:310;anchors_x:330;anchors_y:188}
}
 ##^##*/
