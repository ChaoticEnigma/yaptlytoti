import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

Item {
    id: element
    height: toolButton.height
    anchors.right: parent.right
    anchors.left: parent.left
    anchors.leftMargin: 0

    ToolButton {
        id: toolButton
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 0
        Layout.fillWidth: true

        Label {
            text: model.selected ? ("[" + model.name + "]") : model.name
            anchors.verticalCenter: parent.verticalCenter
            Layout.fillWidth: true
        }

        Image {
            visible: model.voiceEnabled
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            source: "microphone"
        }

    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (mouse.button === Qt.RightButton) {
                menu.popup()
            } else {
                channelsctx.setCurrentChannel(model.id)
            }
        }
    }

    Menu {
        id: menu
        MenuItem {
            text: "Channel Settings"
            onClicked: root.setVisibleView(channelSettings)
        }

        MenuItem {
            text: "Remove Channel"
            onClicked: channelsctx.removeChannel(model.id)
        }
    }

}




