import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

import MessageList 1.0

ScrollView {
    id: chat

    function addMessage() {
        messagesctx.addMessage(chatTextInput.text)
        chatTextInput.text = ""
    }

    ToolBar {
        id: toolBar
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 0

        RowLayout {
            id: rowLayout
            anchors.fill: parent

            TextField {
                id: chatTextInput
                Layout.fillWidth: true
                selectByMouse: true

                Keys.onPressed: {
                    if (event.key === Qt.Key_Enter || event.key === Qt.Key_Return) {
                        addMessage()
                        event.accepted = true
                    }
                }
            }

            ToolButton {
                id: submitButton
                onClicked: {
                    addMessage()
                }

                Image {
                    anchors.centerIn: parent
                    source: "go-next"
                }
            }
        }
    }

    ListView {
        id: chatList
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: toolBar.top
        anchors.top: parent.top
        anchors.topMargin: 0
        clip: true
        model: MessageListModel {
            messages: messagesctx
            onMessagesChanged: {
                chatList.positionViewAtEnd()
            }
        }

        delegate: ChatItem  {}
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:2;anchors_height:100;anchors_width:100}
}
 ##^##*/
