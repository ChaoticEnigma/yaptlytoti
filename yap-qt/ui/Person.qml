import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

ToolButton {
    text: "Person " + model.name

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: {
            if (mouse.button === Qt.RightButton) {
                menu.popup()
            }
        }
    }

    Menu {
        id: menu

        MenuItem {
            text: "Remove Person"
            onClicked: peoplectx.removePerson(model.id)
        }
    }
}

/*##^## Designer {
    D{i:2;anchors_x:33}D{i:3;anchors_width:310;anchors_x:330;anchors_y:188}
}
 ##^##*/
