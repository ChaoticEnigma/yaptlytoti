import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

import ChannelList 1.0

ScrollView {
    id: scrollView
    ToolBar {
        id: footer
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 0

        RowLayout {
            id: rowLayout
            anchors.fill: parent

            TextField {
                id: chatTextInput
                Layout.fillWidth: true
            }

            ToolButton {
                id: submitButton

                Image {
                    anchors.centerIn: parent
                    source: "search"
                }
            }

        }
    }

    ListView {
        id: channelList
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: footer.top
        anchors.top: parent.top
        clip: true
        model: ChannelListModel {
            channels: channelsctx
        }
        delegate: ChannelItem {
            width: channelList.width
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:2;anchors_height:100;anchors_width:100}
}
 ##^##*/
