import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.2

ToolBar {
    id: toolBar

    ToolButton {
        id: channelButton
        visible: root.width < minChannelWidth
        onClicked: root.setVisibleView(channelView)

        Image {
            anchors.centerIn: parent
            source: "globe"
        }
    }

    Label {
        id: channelTitle
        text: channelsctx.currentChannel
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
    }

    ToolButton {
        id: settings
        anchors.right: peopleButton.left
        onClicked: root.setVisibleView(clientSettings)

        Image {
            anchors.centerIn: parent
            source: "configure"
        }
    }

    ToolButton {
        id: peopleButton
        anchors.right: parent.right
        visible: root.width < minPeopleWidth
        onClicked: root.setVisibleView(peopleView)

        Image {
            anchors.centerIn: parent
            source: "im-user"
        }
    }
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
