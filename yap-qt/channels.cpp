#include "channels.h"
#include "servers.h"
#include <QDebug>

Channels::Channels(Servers *servers, Client *aclient) : client(aclient)
{
    connect(servers, &Servers::serverChanged, this, [=](QString id) {
        updateChannelList(id);
    });

    currentServer = client->settings.value(CURRENT_SERVER, "").toString();

    if (currentServer == "") {
        return;
    }

    getChannels();
}

void Channels::addChannel(QString id)
{
    // TODO do serverside
}

void Channels::setCurrentChannel(QString id)
{
    qDebug() << "Setting current channel to: " << id;

    for (int i = 0; i < channels.size(); i++) {
        auto channel = channels[i];
        if (channel.selected && channel.id != id) {
            channel.selected = false;
            updateItemAtIndex(i, channel);
        }
        if (!channel.selected && channel.id == id) {
            channel.selected = true;
            updateItemAtIndex(i, channel);

            emitCurrentChannel(channel.id, channel.name);
        }
    }
}

void Channels::updateItemAtIndex(int i, Channel channel)
{
    // not sure what the correct way to just update an item
    emit preItemRemoved(i, i);
    channels.removeAt(i);
    emit postItemRemoved();
    emit preItemAppended(i, i);
    channels.insert(i, channel);
    emit postItemAppended();
}

void Channels::getChannels()
{
    // TODO do serverside

    QMap<QString, QVariant> serverToChannel = client->settings.value(CURRENT_CHANNEL, {}).toMap();
    channels.append({"A", currentServer + "fakeA", serverToChannel[currentServer] == "A", true});
    channels.append({"B", currentServer + "fakeB", serverToChannel[currentServer] == "B", true});
    channels.append({"C", currentServer + "fakeC", serverToChannel[currentServer] == "C", false});

    // if none of the channels are selected, the select the first channel
    bool selected = false;
    for (auto channel : channels) {
        selected |= channel.selected;

        if (channel.selected) {
            emitCurrentChannel(channel.id, channel.name);
        }
    }
    if (!selected) {
        channels[0].selected = true;
        emitCurrentChannel(channels[0].id, channels[0].name);
    }
}

void Channels::emitCurrentChannel(QString id, QString name)
{
    channel = name;

    // save currently selected channel
    QMap<QString, QVariant> serverToChannel = client->settings.value(CURRENT_CHANNEL, {}).toMap();
    serverToChannel[currentServer] = id;
    client->settings.setValue(CURRENT_CHANNEL, serverToChannel);

    emit channelChanged(id);
}

QVector<Channel> Channels::items() const
{
    return channels;
}

QString Channels::currentChannel() const
{
    return channel;
}

bool Channels::setItemAt(int index, const Channel &channel)
{
    if (index < 0 || index >= channels.size()) {
        return false;
    }

    const Channel &oldChannel = channels[index];

    // return false if nothing has changed
    if (channel.id == oldChannel.id &&
            channel.name == oldChannel.name &&
            channel.selected == oldChannel.selected &&
            channel.voiceEnabled == oldChannel.voiceEnabled) {
        return false;
    }

    channels[index] = channel;
    return true;
}

void Channels::removeChannel(QString id)
{
    for (int i = 0; i < channels.size();) {
        if (channels[i].id == id) {
            // if removing channel currently active,
            // then select a different channel
            if (channels[i].selected) {
                // select 1st channel, but
                // if the 1st channel is being removed
                // then select 2nd channel if available
                if (i != 0) {
                    setCurrentChannel(channels[0].id);
                } else if (channels.size() > 1) {
                    setCurrentChannel(channels[1].id);
                } else {
                    emitCurrentChannel("", "");
                }
            }

            emit preItemRemoved(i, i);

            channels.removeAt(i);

            emit postItemRemoved();
        } else {
            i++;
        }
    }
}

void Channels::updateChannelList(QString id)
{
    currentServer = id;

    // TODO fetch channels from server

    if (!channels.isEmpty()) {
        emit preItemRemoved(0, channels.size() - 1);

        channels = {};

        emit postItemRemoved();
    }

    if (currentServer != "") {
        emit preItemAppended(0, 2);

        getChannels();

        emit postItemAppended();
    } else {
        channel = "";
        emit channelChanged("");
    }
}
