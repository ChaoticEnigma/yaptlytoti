#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include "client.h"
#include <QStringListModel>

class ClientSettings : public QObject {
    Q_OBJECT

public:
    explicit ClientSettings(Client *client);
    ~ClientSettings();

    Q_INVOKABLE QStringList getInputDevices();
    Q_INVOKABLE QStringList getOutputDevices();

    Q_INVOKABLE int getCurrentInputDeviceIndex();
    Q_INVOKABLE int getCurrentOutputDeviceIndex();

    Q_INVOKABLE void setCurrentInputDeviceIndex(int);
    Q_INVOKABLE void setCurrentOutputDeviceIndex(int);

private:
    QList<QAudioDeviceInfo> getInputDevicesInfo();
    QList<QAudioDeviceInfo> getOutputDevicesInfo();

private slots:
    void captureCodecChanged(QString text);

signals:
    void captureCodecChange();

private:
    Client *client;
    QAudioDeviceInfo inputDeviceInfo;
    QAudioDeviceInfo outputDeviceInfo;
};

#endif // SETTINGSDIALOG_H
