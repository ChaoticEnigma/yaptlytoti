#include "serverlistmodel.h"
#include "servers.h"

ServerListModel::ServerListModel(QObject *parent)
    : QAbstractListModel(parent)
    , serverList(nullptr)
{
}

int ServerListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !serverList)
        return 0;

    return serverList->items().size();
}

QVariant ServerListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !serverList)
        return QVariant();

    const Server server = serverList->items()[index.row()];
    switch(role) {
    case IDRole:
        return QVariant(server.id);
    case NameRole:
        return QVariant(server.name);
    case ThumbnailRole:
        return QVariant(server.thumbnail);
    case SelectedRole:
        return QVariant(server.selected);
    }
    return QVariant();
}

bool ServerListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!serverList) {
        return false;
    }

    Server server = serverList->items()[index.row()];
    switch(role) {
    case IDRole:
        server.id = value.toString();
        break;
    case NameRole:
        server.name = value.toString();
        break;
    case ThumbnailRole:
        server.thumbnail = value.toString();
        break;
    case SelectedRole:
        server.selected = value.toBool();
        break;
    }

    if (serverList->setItemAt(index.row(), server)) {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags ServerListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> ServerListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[IDRole] = "id";
    names[NameRole] = "name";
    names[ThumbnailRole] = "thumbnail";
    names[SelectedRole] = "selected";
    return names;
}

Servers *ServerListModel::servers() const
{
    return serverList;
}

void ServerListModel::setServers(Servers *newServers)
{
    beginResetModel();

    if (serverList) {
        serverList->disconnect(this);
    }

    serverList = newServers;

    if (serverList) {
        connect(serverList, &Servers::preItemAppended, this, [=](int index) {
            beginInsertRows(QModelIndex(), index, index);
        });
        connect(serverList, &Servers::postItemAppended, this, [=]() {
            endInsertRows();
        });

        connect(serverList, &Servers::preItemRemoved, this, [=](int index) {
            beginRemoveRows(QModelIndex(), index, index);
        });
        connect(serverList, &Servers::postItemRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}
