#include "peoplelistmodel.h"
#include "people.h"

PeopleListModel::PeopleListModel(QObject *parent)
    : QAbstractListModel(parent)
    , peopleList(nullptr)
{
}

int PeopleListModel::rowCount(const QModelIndex &parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !peopleList)
        return 0;

    return peopleList->items().size();
}

QVariant PeopleListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || !peopleList)
        return QVariant();

    const Person person = peopleList->items()[index.row()];
    switch(role) {
    case IDRole:
        return QVariant(person.id);
    case NameRole:
        return QVariant(person.name);
    }
    return QVariant();
}

bool PeopleListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!peopleList) {
        return false;
    }

    Person person = peopleList->items()[index.row()];
    switch(role) {
    case IDRole:
        person.id = value.toString();
        break;
    case NameRole:
        person.name = value.toString();
        break;
    }

    if (peopleList->setItemAt(index.row(), person)) {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags PeopleListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEditable;
}

QHash<int, QByteArray> PeopleListModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[IDRole] = "id";
    names[NameRole] = "name";
    return names;
}

People *PeopleListModel::people() const
{
    return peopleList;
}

void PeopleListModel::setPeople(People *newPeople)
{
    beginResetModel();

    if (peopleList) {
        peopleList->disconnect(this);
    }

    peopleList = newPeople;

    if (peopleList) {
        connect(peopleList, &People::preItemAppended, this, [=](int start, int end) {
            beginInsertRows(QModelIndex(), start, end);
        });
        connect(peopleList, &People::postItemAppended, this, [=]() {
            endInsertRows();
        });

        connect(peopleList, &People::preItemRemoved, this, [=](int start, int end) {
            beginRemoveRows(QModelIndex(), start, end);
        });
        connect(peopleList, &People::postItemRemoved, this, [=]() {
            endRemoveRows();
        });
    }

    endResetModel();
}
