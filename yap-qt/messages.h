#ifndef MESSAGES_H
#define MESSAGES_H

#include "client.h"
#include <QVector>

class Channels;

struct Message
{
    QString id;
    QString name;
    QString text;
    QString date;
};

class Messages : public QObject
{
    Q_OBJECT
public:
    explicit Messages(Channels *channels, Client *client);

    // allow qml to get messages
    QVector<Message> items() const;

    // allow model to modify messages list
    bool setItemAt(int index, const Message &message);

signals:
    // qml signals
    void preItemAppended(int start, int end);
    void postItemAppended();

    void preItemRemoved(int start, int end);
    void postItemRemoved();

public slots:
    void addMessage(QString);
    // server admins and message owner only
    void removeMessage(QString);
    // message owner only
    void editMessage(QString id, QString text);

private:
    Client *client;

    QVector<Message> messages;

    // channel id
    void updateMessageList(QString id);
    void updateItemAtIndex(int, Message);
    void getMessages();

    QString currentChannel;
    QMap<QString, QVariant> currentChannels;

    // remove when server is real
    int testTmpId = 0;
};

#endif // MESSAGES_H
