#include <unistd.h>
#include <stdio.h>
#include "enet/enet.h"

#include "server.h"

int main(int argc, char **argv){
    if(enet_initialize() != 0){
        printf("Failed initialize enet\n");
        return EXIT_FAILURE;
    }
    atexit(enet_deinitialize);

    ENetAddress address;
    address.host = ENET_HOST_ANY;
//    memset(&address.host, 0, sizeof(address.host));
    address.port = 1234;

    printf("Starting server\n");

    ENetHost *server;
    server = enet_host_create(&address, 32, 2, 0, 0);

    if(server == nullptr){
        printf("Failed to create host\n");
        return EXIT_FAILURE;
    }

    printf("Server listening...\n");

    ENetEvent event;
    while(true){
        // Wait for events
        while (enet_host_service(server, &event, 1000) > 0){
            switch (event.type){
                case ENET_EVENT_TYPE_CONNECT:
                    printf("%x:%u connected\n",
                           event.peer->address.host,
                           event.peer->address.port);
                    event.peer->data = nullptr;
                    break;

                case ENET_EVENT_TYPE_RECEIVE:
                    printf("A packet of length %u containing %s was received from %s on channel %u.\n",
                           event.packet->dataLength,
                           event.packet->data,
                           event.peer->data,
                           event.channelID);
                    /* Clean up the packet now that we're done using it. */
                    enet_packet_destroy (event.packet);
                    break;

                case ENET_EVENT_TYPE_DISCONNECT:
                    printf ("%x:%u disconnected\n",
                            event.peer->address.host,
                            event.peer->address.port);
                    /* Reset the peer's client information. */
                    event.peer->data = nullptr;
                    break;

                default:
                    break;
            }
        }
    }

    enet_host_destroy(server);

    return 0;
}
