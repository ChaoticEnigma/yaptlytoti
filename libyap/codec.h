#ifndef CODEC_H
#define CODEC_H

#include <stdint.h>

typedef enum {
    YAP_CODEC_PCM,
    YAP_CODEC_OPUS,
} yap_codec_type_t;

typedef int (*yap_codec_encode)(const int16_t *samples, uint16_t num_samples);
typedef int (*yap_codec_decode)(int16_t *samples, uint16_t size);

typedef struct {
    yap_codec_type_t type;
    yap_codec_encode encode;
    yap_codec_decode decode;
} yap_codec_t;

#endif // CODEC_H
