## YAPTLYTOTI CMakeLists.txt
CMAKE_MINIMUM_REQUIRED(VERSION 3.0)

PROJECT(YapTlyToti C CXX)

OPTION(YAP_CLIENT "Build Yap Client" ON)
OPTION(YAP_SERVER "Build Yap Server" OFF)

add_subdirectory(libyap)
add_subdirectory(yapc)
add_subdirectory(yap-qt)
add_subdirectory(yapd)
