#include <unistd.h>
#include <stdio.h>
#include "enet/enet.h"

#define TIMEOUT 1000

int main(int argc, char **argv){
    if(enet_initialize() != 0){
        printf("Failed initialize enet\n");
        return EXIT_FAILURE;
    }
    atexit(enet_deinitialize);

    ENetHost *client;
    client = enet_host_create(nullptr, 1, 2, 0, 0);

    if(client == nullptr){
        printf("Failed to create host\n");
        return EXIT_FAILURE;
    }

    ENetAddress address;
    enet_address_set_host(&address, "localhost");
    address.port = 1234;

    ENetPeer *peer;

    printf("Connecting...");
    fflush(stdout);
    peer = enet_host_connect(client, &address, 2, 0);
    if(peer == NULL){
        printf("Erron connecting to peer\n");
        return EXIT_FAILURE;
    }

    ENetEvent event;
    if(enet_host_service(client, &event, 0) > 0 &&
            event.type == ENET_EVENT_TYPE_CONNECT){
        printf("success\n");
        fflush(stdout);
    } else {
        enet_peer_reset(peer);
        printf("failed\n");
    }

    ENetPacket * packet = enet_packet_create ("packet", strlen("packet") + 1, ENET_PACKET_FLAG_RELIABLE);
    enet_peer_send (peer, 0, packet);

    if(enet_host_service(client, &event, 0) > 0 &&
            event.type == ENET_EVENT_TYPE_RECEIVE){
        printf("success\n");
    } else {
        printf("failed\n");
    }


    printf("Disconnecting...");
    fflush(stdout);
    enet_peer_disconnect(peer, 0);

    if(enet_host_service(client, &event, 0) > 0 &&
            event.type == ENET_EVENT_TYPE_DISCONNECT){
        printf("done\n");
    } else {
        enet_peer_reset(peer);
        printf("failed\n");
    }

    enet_host_destroy(client);

    return 0;
}
